var colors = ['red', 'blue'];
console.log(`  🚀: colors`, colors);

// update giá trị của 1 PHẦN TỬ trong ARRAY
// index ~ vị trí ~  dùng để truy xuất tới 1 phần tử trong array
colors[0] = 'black';
console.log(`  🚀: colors`, colors);

// thêm 1 phần tử
colors.push('yellow');
console.log(`  🚀: colors`, colors);

// tìm vị trí của phần tử khi biết giá trị

var viTri = colors.indexOf('black');

console.log(`  🚀: viTri`, viTri);

colors[viTri] = 'super black';
console.log(`  🚀: colors`, colors);

for (var i = 0; i < colors.length; i++) {
  if (colors[i] == 'yellow') {
    colors[i] = 'super yellow';
  }
}
console.log(`  🚀: colors`, colors);
// trả về -1  nếu ko tìm thấy
var viTri2 = colors.indexOf('orange');
console.log(`  🚀: viTri2`, viTri2);
if (viTri2 != -1) {
  colors[viTri2] = 'supper orange';
}

colors.forEach(function (item, index) {
  console.log(`  🚀: item nè`, item, index);
});
