var h1El = document.querySelector('.title');

// querySelector : dom theo giá trị selector ( tương tự khi code css) , chỉ trả về đối tượng đầu tiên nếu tìm thấy
h1El.style.color = 'red';

// lấy danh sách ~ querrySelectorAll

var h1List = document.querySelectorAll('h1');
console.log(`  🚀: h1List`, h1List);

h1List[0].style.color = 'green';
// h1List[0] => index 0 ~ vị trí, index luôn luôn bắt đầu từ 0

h1List[2].style.color = 'blue';

// lấy số lượng của danh sách
var length = h1List.length;
console.log(`  🚀: length`, length);

// convert tất cả h1 thành màu blue

for (var index = 0; index < h1List.length; index++) {
  if (index % 2 == 1) {
    h1List[index].style.color = 'blue';
  }
}

document.querySelector('.footer h1').style.color = 'red';
